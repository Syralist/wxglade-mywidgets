"""\
Lisp generator functions for wxSpinCtrlDouble objects

@copyright: 2002-2004 D. H. aka crazyinsomniac on sourceforge
@copyright: 2014-2016 Carsten Grohmann
@copyright: 2016 Thomas Helmke
@license: MIT (see LICENSE.txt) - THIS PROGRAM COMES WITH NO WARRANTY
"""

import common
import wcodegen


class LispSpinCtrlDoubleGenerator(wcodegen.LispWidgetCodeWriter):
    tmpl = '(setf %(name)s (%(klass)s_Create %(parent)s %(id)s %(value)s ' \
           '-1 -1 -1 -1 %(style)s %(minValue)s %(maxValue)s %(value)s))\n'
    set_default_style = True

    def _prepare_tmpl_content(self, obj):
        wcodegen.LispWidgetCodeWriter._prepare_tmpl_content(self, obj)
        prop = obj.properties
        self.tmpl_dict['value'] = prop.get('value', '')
        try:
            minValue, maxValue = [s.strip() for s in
                                  prop.get('range', '0, 100').split(',')]
        except:
            minValue, maxValue = '0', '100'
        self.tmpl_dict['minValue'] = minValue
        self.tmpl_dict['maxValue'] = maxValue
        return

# end of class LispSpinCtrlDoubleGenerator


def initialize():
    klass = 'wxSpinCtrlDouble'
    common.class_names['EditSpinCtrlDouble'] = klass
    common.register('lisp', klass, LispSpinCtrlDoubleGenerator(klass))
